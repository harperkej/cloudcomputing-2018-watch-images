package cloudcomputing.assignment2.exception;


public enum ExceptionReason {

    RESOURCE_NOT_FOUND("No image found for the watch with the provided sku."),
    UNKNOWN("Unexpected error.");

    private String reason;

    ExceptionReason(String casue) {
        this.reason = casue;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
