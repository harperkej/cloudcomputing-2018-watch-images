package cloudcomputing.assignment2.exception;

public class ApplicationException extends Exception {

    private String message;
    private ExceptionReason casue;

    public ApplicationException(String message, ExceptionReason casue) {
        this.message = message;
        this.casue = casue;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ExceptionReason getCasue() {
        return casue;
    }

    public void setCasue(ExceptionReason casue) {
        this.casue = casue;
    }
}
