package cloudcomputing.assignment2.service;

import cloudcomputing.assignment2.exception.ApplicationException;
import cloudcomputing.assignment2.exception.ExceptionReason;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;

@Service
public class ImageService {

    private AmazonS3 awsS3Client;
    private static final String BUCKET = "cloudcomputing-2018/project1/images";

    @PostConstruct
    public void init() throws Exception {
        this.awsS3Client = AmazonS3ClientBuilder.standard()
                .withRegion(Regions.EU_WEST_1)
                .build();
    }

    public byte[] getImage(String sku) throws ApplicationException {
        try {
            GetObjectRequest getObjectRequest = new GetObjectRequest(BUCKET, sku);
            try (S3Object s3ObjectImage = this.awsS3Client.getObject(getObjectRequest)) {
                try (InputStream s3InputStream = s3ObjectImage.getObjectContent()) {
                    return IOUtils.toByteArray(s3InputStream);
                } catch (IOException e) {
                    throw new ApplicationException("Error reading input stream.", ExceptionReason.UNKNOWN);
                }
            } catch (IOException e) {
                throw new ApplicationException("Error reading input stream.", ExceptionReason.UNKNOWN);
            }
        } catch (AmazonServiceException e) {
            if (e.getStatusCode() == 404) {
                throw new ApplicationException("The watch with sku = " + sku + " has no image.", ExceptionReason.RESOURCE_NOT_FOUND);
            }
        } catch (SdkClientException e) {
            throw new ApplicationException("Error from aws client sdk.", ExceptionReason.UNKNOWN);
        }
        throw new ApplicationException("This should not happen.", ExceptionReason.UNKNOWN);
    }

}
