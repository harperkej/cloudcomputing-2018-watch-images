package cloudcomputing.assignment2.util;

final public class EnumUtility {

    /**
     * A method that can be used for enums instead of toString() method.
     * @param enumType
     * @param <T>
     * @return
     */
    public static <T extends Enum<T>> String enumValuesToString(T enumType) {
        String names = "[";
        for (T t : enumType.getDeclaringClass().getEnumConstants()) {
            names += t.name() + ",";
        }
        if (names.endsWith(",")) {
            names = names.substring(0, names.length() - 1);
        }
        names += "]";
        return names;
    }

}
