package cloudcomputing.assignment2.controller;

import cloudcomputing.assignment2.exception.ApplicationException;
import cloudcomputing.assignment2.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("images/v1")
public class ImageController {

    @Autowired
    ImageService imageService;

    @RequestMapping(value = "get/{sku}", method = RequestMethod.GET, produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<byte[]> getImage(@PathVariable("sku") String sku) throws ApplicationException {
        byte[] image = this.imageService.getImage(sku);
        return new ResponseEntity(image, HttpStatus.OK);
    }

}
