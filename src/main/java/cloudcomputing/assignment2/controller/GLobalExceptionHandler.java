package cloudcomputing.assignment2.controller;

import cloudcomputing.assignment2.dto.ExceptionResponseDto;
import cloudcomputing.assignment2.exception.ApplicationException;
import cloudcomputing.assignment2.exception.ExceptionReason;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;

@ControllerAdvice
public class GLobalExceptionHandler {

    @ResponseBody
    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<?> handleGLobalExceptions(ApplicationException appException) {

        if (appException.getCasue() == ExceptionReason.RESOURCE_NOT_FOUND) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else if (appException.getCasue() == ExceptionReason.UNKNOWN) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
