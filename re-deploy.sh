#!/bin/bash

current_version=`cat version.txt`
echo "Deploying the images-v1 service with version: $current_version"
kubectl set image deployment images-v1 images-v1=harperkej/images-v1:"$current_version"
