#!/bin/bash

echo 'Initial deployment of the service images-v1'
kubectl create -f images-v1-deployment.yaml

echo 'Exposing the images-v1 deployment as a service internally in the cluster.'
kubectl expose deployment images-v1 --target-port=80 --type=NodePort

