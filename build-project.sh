#!/bin/bash

echo '********************** Building the project **********************'
./mvnw clean install -DskipTests=true
echo '********************** Building the project finished **********************'
cp target/assignment2-0.0.1-SNAPSHOT.jar docker/spring-boot-app/
